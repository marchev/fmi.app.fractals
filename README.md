# Sierpinski Triangle Visualization App #


This is a Java desktop app that showcases several different algorithms for generating the [Sierpinski Triangle fractal](https://en.wikipedia.org/wiki/Sierpinski_triangle):

* classic (by removing triangles and rectangles)
* Chaos game
* Iterated function system
* Pascal's triangle.

This is [Svilen Marchev](https://plus.google.com/u/0/+SvilenMarchev/)'s course project for the "Fractals" class, led by prof. Milko Takev at [Faculty of Mathematics and Informatics](https://www.fmi.uni-sofia.bg/en/), Sofia university. The code is as it appeared at the exam in June 2011.

## Technologies ##

* Java
* [G library](http://geosoft.no/graphics/), a layer on top of Java 2D

## Screenshots ##

For more screenshots, see [the screenshots section](https://bitbucket.org/marchev/fmi.app.fractals/src/master/docs/screenshots/?at=master).

![screenshot-triangle.png](https://bitbucket.org/repo/aXegro/images/4232644771-screenshot-triangle.png)

![screenshot-rectangle.png](https://bitbucket.org/repo/aXegro/images/3231259876-screenshot-rectangle.png)

![screenshot-chaos-game.png](https://bitbucket.org/repo/aXegro/images/4205711967-screenshot-chaos-game.png)

## Architecture ##

**For detailed information** about the algorithms, architecture, and technologies used, **refer to [the documentation](https://bitbucket.org/marchev/fmi.app.fractals/src/master/docs/?at=master)**.

![uml-class-diagram.png](https://bitbucket.org/repo/aXegro/images/2838196162-uml-class-diagram.png)