package fmi.fractals.sierpinski;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.geosoft.cc.graphics.GScene;
import no.geosoft.cc.graphics.GWindow;

/**
 * This class manages the Sierpinski triangle demonstration 
 * and contains application's entry point.
 * 
 * @author Svilen
 */
public class Demo extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "Sierpinski Triangle Demo -- by Svilen Marchev";
	private static final String FRACTAL_CHOICE_LABEL = "Fractal:";
	private static final String LEVEL_CHANGE_LABEL = "Level:";
	private static final int WINDOW_HEIGHT = 650;
	private static final int WINDOW_WIDTH = 650;
	
	private GScene scene;
	private JComboBox fractalChoiceCombo;
	private JButton incLevelButton;
	private JButton decLevelButton;
	private JLabel levelChangeLabel;
	private JLabel fractalChoiceLabel;
	
	private JLabel curLevelLabel;
	private JLabel representationLabel;
	
	private FractalsFactory fractalsFactory;
	private String[] fractalsNames;
	private Fractal fractal;
	private int curFractalId;
	
	
	public Demo() {
		super(TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		fractalsFactory = FractalsFactory.getInstance();
		fractalsNames = fractalsFactory.getFractalsNames();
		
		JPanel controlPanel = createControlPanel();
		getContentPane().add(controlPanel, BorderLayout.NORTH);
		
		JPanel statusPanel = createStatusPanel();
		getContentPane().add(statusPanel, BorderLayout.SOUTH);

		// Create the graphic canvas
		GWindow window = new GWindow(new Color(255, 255, 255));
		getContentPane().add(window.getCanvas(), BorderLayout.CENTER);

		// Create scene with default viewport and world extent settings
		scene = new GScene(window);
		double w0[] = { 0.0, 0.0, 0.0 };
		double w1[] = { 1.0, 0.0, 0.0 };
		double w2[] = { 0.0, 1.0, 0.0 };
		scene.setWorldExtent(w0, w1, w2);

		// Create the graphics object and add to the scene
		curFractalId = 0;
		fractal = fractalsFactory.createFractal(curFractalId);
		scene.add(fractal);

		pack();
		setSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
		setResizable(false);
		setVisible(true);

		window.startInteraction(new SquareZoomInteraction(scene));
		refresh();
	}

	/**
	 * Create and return the Control panel.
	 * @return the control panel for the application
	 */
	private JPanel createControlPanel() {
		JPanel controlPanel = new JPanel();
		
		fractalChoiceLabel = new JLabel();
		fractalChoiceLabel.setText(FRACTAL_CHOICE_LABEL);
		controlPanel.add(fractalChoiceLabel);
		
		fractalChoiceCombo = new JComboBox();
		for (int i = 0; i < fractalsNames.length; ++i) {
			fractalChoiceCombo.addItem(fractalsNames[i]);
		}
		fractalChoiceCombo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int newFractalId = ((JComboBox) e.getSource()).getSelectedIndex();
				if (curFractalId != newFractalId) {
					curFractalId = newFractalId;
					scene.remove(fractal);
					fractal = fractalsFactory.createFractal(curFractalId);
					scene.add(fractal);
					scene.redraw();
					refresh();
				}
			}
		});
		controlPanel.add(fractalChoiceCombo);
		
		levelChangeLabel = new JLabel();
		levelChangeLabel.setText("  " + LEVEL_CHANGE_LABEL);
		controlPanel.add(levelChangeLabel);
		
		incLevelButton = new JButton("+");
		incLevelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fractal.incLevel();
				refresh();
			}
		});
		controlPanel.add(incLevelButton);

		decLevelButton = new JButton("-");
		decLevelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fractal.decLevel();
				refresh();
			}
		});
		controlPanel.add(decLevelButton);
		
		return controlPanel;
	}

	/**
	 * Create and return the Status panel.
	 * @return the status panel for the application
	 */
	private JPanel createStatusPanel() {
		JPanel statusPanel = new JPanel();
		
		curLevelLabel = new JLabel();
		statusPanel.add(curLevelLabel);
		
		representationLabel = new JLabel();
		statusPanel.add(representationLabel);

		return statusPanel;
	}
	
	public void refresh() {
		int curLevel = fractal.getLevel();
		curLevelLabel.setText(String.format("Level: %2d", curLevel));
		
		String repr = fractal.getRepresentation();
		representationLabel.setText(repr);
		
		fractal.refresh();
	}

	public static void main(String[] args) {
		new Demo();
	}
}