package fmi.fractals.sierpinski;

import java.awt.Color;
import java.util.ArrayList;

import no.geosoft.cc.graphics.GImage;
import no.geosoft.cc.graphics.GSegment;
import no.geosoft.cc.graphics.GStyle;

/**
 * Represents a Sierpinski triangle fractal which will be drawn 
 * using points only.
 * 
 * @author Svilen
 */
public abstract class SierpinskiWithPoints extends Fractal {
	
	// How many points will be added to the scene on each level
	static final int POINTS_PER_LEVEL_CNT = 200;
	static final boolean ANTIALIASED = false;
	static final GImage MEDIUM_POINT_IMAGE = new GImage(2, 2, new int[] { 1, 1, 1, 1 });
	static final GImage SMALL_POINT_IMAGE = new GImage(1, 1, new int[] { 1 });
		
	// List of all points that should be on the screen
	ArrayList<double[]> pointsCoords;
	// List of all points which are currently on the screen
	ArrayList<GSegment> addedSegments;
	// The image for a single point
	GImage pointImage;

	
	public SierpinskiWithPoints() {
		GStyle style = new GStyle();
		style.setBackgroundColor(Fractal.FRACTAL_COLOR);
		style.setLineStyle(GStyle.LINESTYLE_SOLID);
		style.setAntialiased(SierpinskiWithPoints.ANTIALIASED);
		setStyle(style);

		pointsCoords = new ArrayList<double[]>();
		addedSegments = new ArrayList<GSegment>();
		curLevel = 0;
		
		GStyle symbolStyle = new GStyle();
		symbolStyle.setForegroundColor(FRACTAL_COLOR);
		symbolStyle.setBackgroundColor(new Color(255, 255, 255));
		pointImage = MEDIUM_POINT_IMAGE;
		pointImage.setStyle(symbolStyle);
	}
	
	int getPointsCount() {
		return pointsCoords.size();
	}
	
	@Override
	synchronized public void draw() {
		// Add new points to the scene
		while (addedSegments.size() < pointsCoords.size()) {
			GSegment s = new GSegment();
			addSegment(s);
			s.setVertexImage(pointImage);
			s.setGeometryXy(pointsCoords.get(addedSegments.size()));
			addedSegments.add(s);
		}

		// Remove points from scene if they are deleted by the user
		while (addedSegments.size() > pointsCoords.size()) {
			removeSegment(addedSegments.size() - 1);
			addedSegments.remove(addedSegments.size() - 1);
		}
		
		// Redraw all points
		for (int i = 0; i < addedSegments.size(); ++i) {
			addedSegments.get(i).setGeometryXy(pointsCoords.get(i));
		}
	}
	
	@Override
	synchronized public void incLevel() {
		curLevel += 1;
		
		double[] lastPnt = pointsCoords.isEmpty() ? getInitialPoint()
				: pointsCoords.get(pointsCoords.size() - 1);
		
		pointsCoords.ensureCapacity(pointsCoords.size() + POINTS_PER_LEVEL_CNT);
		for (int i = 0; i < POINTS_PER_LEVEL_CNT; ++i) {
			double[] curPnt = computeNextPoint(lastPnt);
			pointsCoords.add(curPnt);
			lastPnt = curPnt;
		}
		
		redraw();
	}
	
	@Override
	synchronized public void decLevel() {
		if (curLevel <= 0) {
			return;
		}
		curLevel -= 1;
		
		for (int i = 0; i < POINTS_PER_LEVEL_CNT; ++i) {
			pointsCoords.remove(pointsCoords.size() - 1);
		}
		redraw();
	}

	@Override
	public String getRepresentation() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("    Points: %3d", getPointsCount()));
		return sb.toString();
	}
	
	abstract double[] getInitialPoint();
	
	abstract double[] computeNextPoint(double[] lastPnt);
}
