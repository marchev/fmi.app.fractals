package fmi.fractals.sierpinski;

import java.util.Random;

/**
 * This class represents a Sierpinski triangle fractal which is drawn 
 * using points using Iterated function system and the Chaos game method.
 * 
 * http://en.wikipedia.org/wiki/Sierpinski_triangle
 * 
 * @author Svilen
 */
public class SierpinskiIfs extends SierpinskiWithPoints {

	private static final double INIT_SIDE_SIZE = 0.75;
	private static final double INIT_X_COORD = (1.0 - INIT_SIDE_SIZE) / 2;
	private static final double INIT_Y_COORD = 0.2;

	Random random = new Random(16051988);
	
	public SierpinskiIfs() {
		super();
	}
	
	@Override
	double[] getInitialPoint() {
		// Return the bottom left point of the triangle
		return new double[] { INIT_X_COORD, INIT_Y_COORD };
	}
	
	@Override
	double[] computeNextPoint(double[] lastPnt) {
		double[] lp = { (lastPnt[0] - INIT_X_COORD) / INIT_SIDE_SIZE,
				(lastPnt[1] - INIT_Y_COORD) / INIT_SIDE_SIZE };
		double[] pnt = new double[2];
		
		int transformId = random.nextInt(3);
		switch (transformId) {
		case 0:
			pnt[0] = lp[0] / 2;
			pnt[1] = lp[1] / 2;
			break;
		case 1:
			pnt[0] = lp[0] / 2 + 0.25;
			pnt[1] = lp[1] / 2 + Math.sqrt(3.0) / 4;
			break;
		case 2:
			pnt[0] = lp[0] / 2 + 0.5;
			pnt[1] = lp[1] / 2;
			break;
		}
		
		pnt[0] = pnt[0] * INIT_SIDE_SIZE + INIT_X_COORD;
		pnt[1] = pnt[1] * INIT_SIDE_SIZE + INIT_Y_COORD;
		return pnt;
	}
}
