package fmi.fractals.sierpinski;


/**
 * A class representing the Sierpinski triangle fractal which is drawn 
 * using triangles.
 * 
 * @author Svilen
 */
public class SierpinskiTriangle extends SierpinskiWithShapes {
	private static final double INIT_SIDE_SIZE = 0.75;
	private static final double INIT_Y_COORD = 0.2;
	
	public SierpinskiTriangle() {
		super();
	}
	
	@Override
	double[][] generateInitGeometries() {
		return new double[][] {{
			0.5, INIT_Y_COORD + INIT_SIDE_SIZE * Math.sin(Math.PI / 3.0),
			0.5 - INIT_SIDE_SIZE / 2, INIT_Y_COORD,
			0.5 + INIT_SIDE_SIZE / 2, INIT_Y_COORD,
			0.5, INIT_Y_COORD + INIT_SIDE_SIZE * Math.sin(Math.PI / 3.0),				
		}};
	}
	
	@Override
	double getArea() {
		double initTriangleArea = INIT_SIDE_SIZE * INIT_SIDE_SIZE * Math.sqrt(3.0) / 4.0;
		return Math.pow(3.0 / 4.0, curLevel) * initTriangleArea;
	}
	
	@Override
	int getShapesCount() {
		int cnt = 1;
		for (int i = 0; i < curLevel; ++i) {
			cnt *= 3;
		}
		return cnt;
	}
	
	@Override
	public String getRepresentation() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("    Area: %.5f", getArea()));
		sb.append(String.format("    Triangles: %2d", getShapesCount()));
		return sb.toString();
	}
	
	@Override
	synchronized public void incLevel() {
		curLevel += 1;
		
		clearShapes();
		
		double[][] geoms = new double[3 * geometriesCoords.length][];
		for (int i = 0; i < geometriesCoords.length; ++i) {
			double[] g = geometriesCoords[i];
			geoms[3 * i] = new double[] {
				g[0], g[1],
				g[0] - (g[0] - g[2]) / 2, g[1] - (g[1] - g[3]) / 2,
				g[0] + (g[0] - g[2]) / 2, g[1] - (g[1] - g[3]) / 2,
				g[0], g[1],
			};
			geoms[3 * i + 1] = new double[] {
				geoms[3 * i][2], geoms[3 * i][3],
				g[2], g[3],
				g[0], g[3],
				geoms[3 * i][2], geoms[3 * i][3],
			};
			geoms[3 * i + 2] = new double[] {
				geoms[3 * i][4], geoms[3 * i][5],
				g[0], g[3],
				g[4], g[5],
				geoms[3 * i][4], geoms[3 * i][5],
			};
		}
		geometriesCoords = geoms;
		redraw();
	}
	
	@Override
	synchronized public void decLevel() {
		if (curLevel <= 0) {
			return;
		}
		curLevel -= 1;
		
		clearShapes();
		
		double[][] geoms = new double[geometriesCoords.length / 3][];
		for (int i = 0; i < geometriesCoords.length; i += 3) {
			geoms[i / 3] = new double[] {
				geometriesCoords[i][0], geometriesCoords[i][1],
				geometriesCoords[i + 1][2], geometriesCoords[i + 1][3],
				geometriesCoords[i + 2][4], geometriesCoords[i + 2][5],
				geometriesCoords[i][0], geometriesCoords[i][1],
			};
		}
		geometriesCoords = geoms;
		redraw();
	}
}
