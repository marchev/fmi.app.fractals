package fmi.fractals.sierpinski;

/**
 * Abstract factory and Singleton patterns for creation of fractals.
 * Using this factory the user code, which needs a fractal instance,
 * do not need to know anything about the concrete Fractal classes. 
 * 
 * @author Svilen
 */
public class FractalsFactory {
	private static final FractalsFactory INSTANCE = new FractalsFactory();
	
	private static final String[] FRACTALS_NAMES = {
		"Sierpinski triangle",
		"Sierpinski triangle (from rectangle)",
		"Sierpinski triangle (Chaos game method)",
		"Sierpinski triangle (Iterated function system method)",
	};
	
	private FractalsFactory() {
	}
	
	public static FractalsFactory getInstance() {
		return INSTANCE;
	}
	
	public Fractal createFractal(int id) {
		Fractal fr = null;
		switch (id) {
		case 0:
			fr = new SierpinskiTriangle();
			break;
		case 1:
			fr = new SierpinskiRectangle();
			break;
		case 2:
			fr = new SierpinskiChaosGame();
			break;
		case 3:
			fr = new SierpinskiIfs();
			break;
		default:
			System.err.println("Error: invalid fractal id");
		}
		return fr;
	}
	
	public String[] getFractalsNames() {
		return FRACTALS_NAMES;
	}
}
