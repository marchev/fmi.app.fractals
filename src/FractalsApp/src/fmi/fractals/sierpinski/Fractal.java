package fmi.fractals.sierpinski;

import java.awt.Color;

import no.geosoft.cc.graphics.GObject;

/**
 * Common behaviour for all fractals.
 * 
 * @author Svilen
 *
 */
public abstract class Fractal extends GObject {
	static Color FRACTAL_COLOR = new Color(0, 255, 0);
	
	int curLevel;
	
	public int getLevel() {
		return curLevel;
	}
	
	abstract public void incLevel();
	
	abstract public void decLevel();
	
	abstract public String getRepresentation();
}
