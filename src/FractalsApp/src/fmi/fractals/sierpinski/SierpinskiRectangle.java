package fmi.fractals.sierpinski;

/**
 * A class representing the Sierpinski rectangle fractal which is drawn 
 * using rectangles.
 * 
 * @author Svilen
 */
public class SierpinskiRectangle extends SierpinskiWithShapes {
	private static final double INIT_SIDE_SIZE = 0.75;
	
	public SierpinskiRectangle() {
		super();
	}
	
	@Override
	double[][] generateInitGeometries() {
		return new double[][] {{
			0.5 - INIT_SIDE_SIZE / 2, 0.5 + INIT_SIDE_SIZE / 2,
			0.5 - INIT_SIDE_SIZE / 2, 0.5 - INIT_SIDE_SIZE / 2,
			0.5 + INIT_SIDE_SIZE / 2, 0.5 - INIT_SIDE_SIZE / 2,
			0.5 + INIT_SIDE_SIZE / 2, 0.5 + INIT_SIDE_SIZE / 2,
			0.5 - INIT_SIDE_SIZE / 2, 0.5 + INIT_SIDE_SIZE / 2,
		}};
	}
	
	@Override
	double getArea() {
		double initRectArea = INIT_SIDE_SIZE * INIT_SIDE_SIZE;
		return Math.pow(3.0 / 4.0, curLevel) * initRectArea;
	}
	
	@Override
	int getShapesCount() {
		int cnt = 1;
		for (int i = 0; i < curLevel; ++i) {
			cnt *= 3;
		}
		return cnt;
	}
	
	@Override
	public String getRepresentation() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("    Area: %.5f", getArea()));
		sb.append(String.format("    Rectangles: %2d", getShapesCount()));
		return sb.toString();
	}
	
	@Override
	synchronized public void incLevel() {
		curLevel += 1;
		
		clearShapes();
		
		double[][] geoms = new double[3 * geometriesCoords.length][];
		for (int i = 0; i < geometriesCoords.length; ++i) {
			double[] g = geometriesCoords[i];
			double side2 = (g[1] - g[3]) / 2;
			double x0 = g[0] + side2 / 2;
			double y0 = g[1];
			double x1 = g[0];
			double y1 = g[1] - side2;
			double x2 = g[0] + side2;
			double y2 = g[1] - side2;

			geoms[3 * i] = new double[] {
				x0, y0,
				x0, y0 - side2,
				x0 + side2, y0 - side2,
				x0 + side2, y0,
				x0, y0,
			};
			geoms[3 * i + 1] = new double[] {
				x1, y1,
				x1, y1 - side2,
				x1 + side2, y1 - side2,
				x1 + side2, y1,
				x1, y1,
			};
			geoms[3 * i + 2] = new double[] {
				x2, y2,
				x2, y2 - side2,
				x2 + side2, y2 - side2,
				x2 + side2, y2,
				x2, y2,
			};
		}
		geometriesCoords = geoms;
		redraw();
	}
	
	@Override
	synchronized public void decLevel() {
		if (curLevel <= 0) {
			return;
		}
		curLevel -= 1;
		
		clearShapes();
		
		double[][] geoms = new double[geometriesCoords.length / 3][];
		for (int i = 0; i < geometriesCoords.length; i += 3) {
			double side = 2 * (geometriesCoords[i][1] - geometriesCoords[i][3]);
			double x = geometriesCoords[i][0] - side / 4;
			double y = geometriesCoords[i][1];
			geoms[i / 3] = new double[] {
				x, y,
				x, y - side,
				x + side, y - side,
				x + side, y,
				x, y,
			};
		}
		geometriesCoords = geoms;
		redraw();
	}
}
