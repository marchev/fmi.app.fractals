package fmi.fractals.sierpinski;

import java.util.ArrayList;
import java.util.Iterator;

import no.geosoft.cc.graphics.GSegment;
import no.geosoft.cc.graphics.GStyle;

/**
 * Represents a Sierpinski triangle fractal which will be drawn 
 * using shapes only (e.g. triangles, rectangles, etc.).
 * 
 * @author Svilen
 */
public abstract class SierpinskiWithShapes extends Fractal {
	static final boolean ANTIALIASED = true;
	
	ArrayList<GSegment> addedShapes;
	double[][] geometriesCoords;

	public SierpinskiWithShapes() {
		GStyle style = new GStyle();
		style.setBackgroundColor(Fractal.FRACTAL_COLOR);
		style.setLineStyle(GStyle.LINESTYLE_INVISIBLE);
		style.setAntialiased(SierpinskiWithShapes.ANTIALIASED);
		setStyle(style);

		geometriesCoords = generateInitGeometries();
		addedShapes = new ArrayList<GSegment>();
		curLevel = 0;
	}
	
	abstract double[][] generateInitGeometries();
	
	abstract double getArea();
	
	abstract int getShapesCount();
	
	void clearShapes() {
		removeSegments();
		addedShapes.clear();
	}
	
	@Override
	synchronized public void draw() {
		boolean shouldAddShapes = addedShapes.isEmpty();
		
		if (shouldAddShapes) {
			addedShapes.ensureCapacity(geometriesCoords.length);
			for (int i = 0; i < geometriesCoords.length; ++i) {
				GSegment s = new GSegment();
				addSegment(s);
				addedShapes.add(s);
				s.setGeometryXy(geometriesCoords[i]);
			}
		} else {
			Iterator<GSegment> triangleIter = addedShapes.iterator();
			for (int i = 0; i < geometriesCoords.length; ++i) {
				GSegment s = triangleIter.next();
				s.setGeometryXy(geometriesCoords[i]);
			}
		}
	}
}
