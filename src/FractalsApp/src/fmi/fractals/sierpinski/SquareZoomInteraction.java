package fmi.fractals.sierpinski;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import no.geosoft.cc.graphics.GInteraction;
import no.geosoft.cc.graphics.GObject;
import no.geosoft.cc.graphics.GScene;
import no.geosoft.cc.graphics.GSegment;
import no.geosoft.cc.graphics.GStyle;
import no.geosoft.cc.graphics.GWindow;

/**
 * A "square" zoom interaction. The following actions are implemented:
 * 
 * <ol>
 * <li>Button 1 click at a point in the scene: The scene is zoomed in a certain
 * factor with that point fixed.
 * <li>Button 1 press and keep pressed: The scene is zoomed in continously until
 * the button is released.
 * <li>Button 3 click at a point in the scene: The scene is zoomed out a certain
 * factor with that point fixed.
 * <li>Button 3 press and keep pressed: The scene is zoomed out continously
 * until the button is released.
 * <li>Button 2 click: Unzoom.
 * <li>Button 1 press, drag and release: User creates a rubber band and the
 * scene is zoomed to the specified *square* area.
 * </ol>
 * 
 * The <code>SquareZoomInteraction</code> class reuses the code of 
 * <code>ZoomInteraction</code>.
 * 
 * @author Svilen
 */
public class SquareZoomInteraction implements GInteraction, ActionListener {

	private static final double ZOOM_FACTOR = 0.9;

	private final GScene scene_;
	private final GObject interaction_;
	private final GSegment rubberBand_;

	private int x_[], y_[];
	private int x0_, y0_;
	private Timer timer_;
	private double zoomFactor_;

	/**
	 * Create a new zoom interaction on the specified scene. The interaction is
	 * activated by GWindow.setInteraction().
	 * 
	 * @see GWindow#startInteraction(GInteraction)
	 * @see GWindow#stopInteraction()
	 * 
	 * @param scene
	 *            Scene of this zoom interaction.
	 * @param style
	 *            Style for the interaction rubber band
	 */
	public SquareZoomInteraction(GScene scene, GStyle style) {
		scene_ = scene;

		// Create a graphic node for holding the interaction graphics
		interaction_ = new GObject("Interaction");

		// Default rubberband style if none provided
		if (style == null) {
			style = new GStyle();
			style.setLineWidth(1);
			style.setForegroundColor(new Color(0, 0, 0));
			style.setBackgroundColor(null);
		}
		interaction_.setStyle(style);

		// Create and attach rubberband segment
		rubberBand_ = new GSegment();
		interaction_.addSegment(rubberBand_);

		// For the rubberband geometry
		x_ = new int[5];
		y_ = new int[5];
	}

	/**
	 * Create a new zoom interaction on the specified scene.
	 * 
	 * @see GWindow#startInteraction(GInteraction)
	 * @see GWindow#stopInteraction()
	 * 
	 * @param scene
	 *            Scene of this zoom interaction.
	 */
	public SquareZoomInteraction(GScene scene) {
		this(scene, null);
	}

	/**
	 * Timer triggered event when the user keeps pressing a mouse button.
	 * 
	 * @param event
	 *            Not used.
	 */
	public void actionPerformed(ActionEvent event) {
		scene_.zoom(x0_, y0_, zoomFactor_);
	}

	/**
	 * Handle mouse events in the canvas.
	 * 
	 * @param eventType
	 *            Event triggering this method.
	 * @param x
	 *            Pointer location.
	 * @param y
	 *            Pointer location.
	 */
	public void event(GScene scene, int eventType, int x, int y) {
		int dx = Math.abs(x - x0_);
		int dy = Math.abs(y - y0_);

		int dmin = (dx <= dy) ? dx : dy;
		int x1 = x0_ + dmin * (x - x0_ < 0 ? -1 : +1);
		int y1 = y0_ + dmin * (y - y0_ < 0 ? -1 : +1);
		
		switch (eventType) {
		case GWindow.BUTTON1_DOWN:
			x0_ = x;
			y0_ = y;

			zoomFactor_ = ZOOM_FACTOR;

			scene_.add(interaction_); // Front

			// Cause actionPerformed() to be called every 90ms as long
			// as button is pressed.
			timer_ = new Timer(90, this);
			timer_.setInitialDelay(500);
			timer_.start();
			break;

		case GWindow.BUTTON1_UP:
			interaction_.remove();
			rubberBand_.setGeometry((int[]) null);

			timer_.stop();
			timer_.removeActionListener(this);

			// If the rubber band is very small, interpret it as a click
			if (dx < 3 || dy < 3) {
				scene_.zoom(x, y, zoomFactor_);
			} else {
				// Else we zoom into the area defined by the rubberband
				scene_.zoom(x0_, y0_, x1, y1);
			}
			break;

		case GWindow.BUTTON2_UP:
			scene_.unzoom();
			break;

		case GWindow.BUTTON1_DRAG:
			timer_.stop();
			timer_.removeActionListener(this);

			x_[0] = x0_;
			x_[1] = x1;
			x_[2] = x1;
			x_[3] = x0_;
			x_[4] = x0_;

			y_[0] = y0_;
			y_[1] = y0_;
			y_[2] = y1;
			y_[3] = y1;
			y_[4] = y0_;

			rubberBand_.setGeometry(x_, y_);
			scene_.refresh();
			break;

		case GWindow.BUTTON3_DOWN:
			x0_ = x;
			y0_ = y;

			zoomFactor_ = 1.0 / ZOOM_FACTOR;

			// Cause actionPerformed() to be called every 90ms as long
			// as button is pressed.
			timer_ = new Timer(90, this);
			timer_.setInitialDelay(500);
			timer_.start();

			break;

		case GWindow.BUTTON3_UP:
			timer_.stop();
			timer_.removeActionListener(this);

			scene_.zoom(x, y, zoomFactor_);
			break;
		}
	}
}
