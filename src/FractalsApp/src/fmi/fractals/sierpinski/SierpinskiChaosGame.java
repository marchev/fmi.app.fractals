package fmi.fractals.sierpinski;

import java.util.Random;

/**
 * This class represents a Sierpinski triangle fractal which is drawn 
 * using points using the Chaos game method.
 * 
 * http://en.wikipedia.org/wiki/Sierpinski_triangle
 * 
 * @author Svilen
 */
public class SierpinskiChaosGame extends SierpinskiWithPoints {

	private static final double INIT_SIDE_SIZE = 0.75;
	private static final double INIT_Y_COORD = 0.2;

	final double[][] trianglePoints = {
		{0.5, INIT_Y_COORD + INIT_SIDE_SIZE * Math.sin(Math.PI / 3.0)},
		{0.5 - INIT_SIDE_SIZE / 2, INIT_Y_COORD},
		{0.5 + INIT_SIDE_SIZE / 2, INIT_Y_COORD},
	};
	
	Random random = new Random(16051988);
	
	public SierpinskiChaosGame() {
		super();
	}
	
	@Override
	double[] getInitialPoint() {
		// Return a points that is *inside* of the triangle
		return trianglePoints[0];
	}
	
	@Override
	double[] computeNextPoint(double[] lastPnt) {
		int k = random.nextInt(3);
		double x = lastPnt[0] + (trianglePoints[k][0] - lastPnt[0]) / 2.0;
		double y = lastPnt[1] + (trianglePoints[k][1] - lastPnt[1]) / 2.0;
		return new double[] { x, y };
	}
}
